.PHONY: run_docker install_ansible run help

run_docker: PLAYBOOKS_PATH=$(realpath .)
run_docker: ## Run bin/sh inside docker container with ansible. Mount all files from the root (only for debugging purpose)
	docker run -v ${PLAYBOOKS_PATH}:/in -it docker.io/cytopia/ansible:latest bin/sh

install_brew: ## Install brew
	/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"

install_ansible: ## Install ansible from brew
	@if [ -z $(shell command -v brew 2>/dev/null) ]; then ${MAKE} install_brew; fi;
	@$(call log_msg,"Installing ansible...")
	brew install ansible

run: ## Run ansible with playbook-apps.yaml on localhost
	@if [ -z $(shell command -v ansible-playbook 2>/dev/null) ]; then ${MAKE} install_brew; fi;
	@$(call log_msg,"Running ansible-playbook...")
	ansible-playbook playbook-apps.yaml -K

.PHONY: help
help: ## Display this help screen
	@grep -h -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'