# Small ansible playbook with initializing apps on new MacBook

| make command| description|
| ----------- | ----------- |
|run_docker  |                   Run bin/sh inside docker container with ansible. Mount all files from the root (only for debugging)|
|install_brew   |                Install brew|
|install_ansible |               Install ansible from brew|
|run         |                   Run ansible with playbook-apps.yaml on localhost|
|help        |                   Display this help screen|

### What should be done / added

- [x] homebrew
- [x] wget
- [x] Docker desktop
- [x] oh my zsh https://gist.github.com/kevin-smets/8568070 
- [x] zsh: add config
- [x] git
- [x] git: add .gitconfig file
- [x] golang 
- [x] golang: set variables 
- [x] GoLand
- [ ] GoLand: add config
- [x] postman
- [x] obsidian
- [x] chrome
- [x] telegram 
- [x] vs code
- [x] tunnelblick
- [ ] search all /bin directories and add to %PATH